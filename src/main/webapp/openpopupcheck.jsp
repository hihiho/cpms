<%--
  Created by IntelliJ IDEA.
  User: Dev307
  Date: 2021-06-07
  Time: 오전 9:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <script>
        /*팝업시 openPopup함수 호출시, 브라우저 팝업차단 여부에 따라 체크 로직*/
        function openPopup(){
            var win = window.open('', 'win', 'width=1, height=1, scrollbars=yes, resizable=yes');

            if (win == null || typeof(win) == "undefined" || (win == null && win.outerWidth == 0) || (win != null && win.outerHeight == 0) || win.test == "undefined")
            {
                alert("팝업 차단 기능이 설정되어있습니다\n\n차단 기능을 해제(팝업허용) 한 후 다시 이용해 주십시오.\n\n만약 팝업 차단 기능을 해제하지 않으면\n정상적인 주문이 이루어지지 않습니다.");

                if(win){
                    win.close();
                }
                return;
            }
            else if (win)
            {
                if (win.innerWidth === 0)
                {
                    alert("팝업 차단 기능이 설정되어있습니다\n\n차단 기능을 해제(팝업허용) 한 후 다
                    시 이용해 주십시오.\n\n만약 팝업 차단 기능을 해제하지 않으면\n정상적인 주문이 이루어지지 않습니다.");
                }
            }
            else
            {
                return;
            }
            if(win){    // 팝업창이 떠있다면 close();
                win.close();
            }
        }    // 함수 끝

        window.onload = function(){      // 페이지 로딩 후 즉시 함수 실행(window.onload)
            openPopup()
        }


        //출처: https://h5bak.tistory.com/194 [이준빈은 호박머리]
    </script>

</body>
</html>
